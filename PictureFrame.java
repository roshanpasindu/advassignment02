import java.awt.*;

import javax.swing.*;

public class PictureFrame {
  public int[] reroll = null;
  Aardvark master = null;

  class DominoPanel extends JPanel {
    private static final long serialVersionUID = 4190229282411119364L;
    public static final int ColourVal = 20;
    public static final int DrawVal = 30;
    public void drawGrid(Graphics g) {
    for (int are = 0; are < 7; are++) {
    for (int see = 0; see < 8; see++) {
    drawDigitGivenCentre(g, DrawVal + see * ColourVal, DrawVal + are *
    ColourVal, ColourVal,
    master.grid[are][see]);
    }
    }
    }
    public void drawGridLines(Graphics g) {
    g.setColor(Color.LIGHT_GRAY);
    for (int are = 0; are <= 7; are++) {
    g.drawLine(ColourVal, ColourVal + are * ColourVal, 180, ColourVal + are * ColourVal);
    }
    for (int see = 0; see <= 8; see++) {
    g.drawLine(ColourVal + see * ColourVal, ColourVal, ColourVal + see *
    ColourVal, 180);
    }
    }
    public void drawHeadings(Graphics g) {
    for (int are = 0; are < 7; are++) {
    fillDigitGivenCentre(g, 10, DrawVal + are * ColourVal, ColourVal, are+1);
    }
    for (int see = 0; see < 8; see++) {
    fillDigitGivenCentre(g, DrawVal + see * ColourVal, 10, ColourVal, see+1);
    }
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.BLACK);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    void drawDigitGivenCentre(Graphics g, int x, int y, int diameter, int n,
        Color c) {
      int radius = diameter / 2;
      g.setColor(c);
      // g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }

    void fillDigitGivenCentre(Graphics g, int x, int y, int diameter, int n) {
      int radius = diameter / 2;
      g.setColor(Color.GREEN);
      g.fillOval(x - radius, y - radius, diameter, diameter);
      g.setColor(Color.BLACK);
      g.drawOval(x - radius, y - radius, diameter, diameter);
      FontMetrics fm = g.getFontMetrics();
      String txt = Integer.toString(n);
      g.drawString(txt, x - fm.stringWidth(txt) / 2, y + fm.getMaxAscent() / 2);
    }
@Override
    protected void paintComponent(Graphics g) {
      g.setColor(Color.YELLOW);
      g.fillRect(0, 0, getWidth(), getHeight());
      if (master.data.mode == 1) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawGuesses(g);
      }
      if (master.data.mode == 0) {
        drawGridLines(g);
        drawHeadings(g);
        drawGrid(g);
        master.drawDominoes(g);
      }
    }
@Override
    public Dimension getPreferredSize() {
      return new Dimension(202, 182);
    }
  }

  public DominoPanel dp;

  public void PictureFrame(Aardvark sf) {
    master = sf;
    if (dp == null) {
      JFrame f = new JFrame("Abominodo");
      dp = new DominoPanel();
      f.setContentPane(dp);
      f.pack();
      f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
      f.setVisible(true);
    }
  }

  public void reset() {
    // TODO Auto-generated method stub

  }

}
