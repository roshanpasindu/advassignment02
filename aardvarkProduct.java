import java.util.LinkedList;
import java.util.List;

public class aardvarkProduct {
	public static final int[][] grid = null;
	private List<Domino> _g;
	public int mode;
	public Object _d;
	public List<Domino> get_g() {
	return _g;
	}
	public void generateGuesses() {
	_g = new LinkedList<Domino>();
	int count = 0;
	int x = 0;
	int y = 0;
	for (int l = 0; l <= 6; l++) {
	for (int h = l; h <= 6; h++) {
	Domino d = new Domino(h, l);
	_g.add(d);
	count++;
	}
	}
	if (count != 28) {
	System.out.println("something went wrong generating dominoes");
	System.exit(0);
	}
	}
	public void printGuesses() {
	for (Domino d : _g) {
	System.out.println(d);
	}
	}
	}
	